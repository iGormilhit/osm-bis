# Les bibliothèques sur OSM

Lors du Congrès [BIS](http://www.bis.ch/fr) 2016, j'ai donné une courte présentation dont l'objectif est de convaincre de l'intérêt d'améliorer les données des bibliothèques sur OpenStreetMap pour la Suisse.

## Ressources

Quelques références : https://www.zotero.org/igor.m/items/tag/osmbis

Quelques liens : https://id-libre.org/shaarli/?searchtags=OpenStreetMap

## Utilisation des sources

1. Télécharger ou cloner le dépôt :
   * télécharger le ZIP et extraire les fichier
   * ou `git clone https://framagit.org/iGormilhit/osm-bis.git`
2. Ouvrir le fichier `index.html` avec un navigateur Web
